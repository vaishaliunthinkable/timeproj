import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
part 'time_event.dart';
part 'time_state.dart';

class TimeBloc extends Bloc<TimeEvent, TimeState> {
  TimeBloc() : super(TimeInitial());

  @override
  Stream<TimeState> mapEventToState(
    TimeEvent event,
  ) async* {
    if (event is MessageEvent) {
      var message;
      if (event.time.hour >=4&&event.time.hour < 12)
        message = 'Morning';
      else if (event.time.hour >=12&&event.time.hour < 16)
        message = 'Afternoon';
      else if (event.time.hour >=16&&event.time.hour < 20)
        message = 'Evening';
      else
        message = 'Night';

      yield MessageState(event.time, message);
    }
  }
}

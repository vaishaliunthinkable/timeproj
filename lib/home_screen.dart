import 'package:flutter/material.dart';
import 'bloc/time_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _bloc = BlocProvider.of<TimeBloc>(context);
      String mes;
    return Scaffold(
        appBar: AppBar(
          title: Text("Time App"),
          backgroundColor: Colors.redAccent,
        ),
        body:
        Center(


          child:

          Column(

            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BlocBuilder<TimeBloc, TimeState>(
                builder: (context, state) {
                  _bloc.add(MessageEvent(DateTime.now()));
                  return

                    Column(
                    children: [
                      Text(
                        (state is MessageState)
                            ? '${state.time.hour.toString().padLeft(2, '0')}:${state.time.minute.toString().padLeft(2, '0')}:${state.time.second.toString().padLeft(2, '0')}'
                            : 'Loading...',
                        style: TextStyle(
                          fontSize: MediaQuery.textScaleFactorOf(context)*60,
                        ),
                      ),
                      Text(
                        (state is MessageState)
                            ? 'Hey! Good ${state.message}'
                            : '',
                        style: TextStyle(
                          fontSize: MediaQuery.textScaleFactorOf(context)*40,
                        ),
                      ),

                      if(state is MessageState && state.message=='Morning')
                      Image.asset(
                        "images/goodmorning.gif",
                        height: MediaQuery.textScaleFactorOf(context)*250,
                        width: MediaQuery.textScaleFactorOf(context)*250,
                      )
                    else  if(state is MessageState && state.message=='Afternoon')
                        Image.asset(
                          "images/goodaft.gif",
                          height: MediaQuery.textScaleFactorOf(context)*250,
                          width: MediaQuery.textScaleFactorOf(context)*250,
                        )
                      else  if(state is MessageState && state.message=='Evening')
                          Image.asset(
                            "images/gev.gif",
                            height: MediaQuery.textScaleFactorOf(context)*250,
                            width: MediaQuery.textScaleFactorOf(context)*250,
                          )
                        else
                            Image.asset(
                              "images/gnight.gif",
                              height: MediaQuery.textScaleFactorOf(context)*250,
                              width: MediaQuery.textScaleFactorOf(context)*250,
                            )

                    ],
                  );
                },
              ),
              // RaisedButton(onPressed: () {
              //   _bloc.add(MessageEvent(DateTime.now()));
              // }),
            ],
          ),
        ));
  }
}
